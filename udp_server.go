package main

import (
	"fmt"
	"github.com/timandy/routine"
	"math/rand"
	"net"
	"os"
	"runtime"
	"strconv"
	"time"
)

func random(min, max int) int {
	return rand.Intn(max-min) + min
}

func listen(connection *net.UDPConn, quit chan struct{}) {
	buffer := make([]byte, 1024)
	n, remoteAddr, err := 0, new(net.UDPAddr), error(nil)
	//while there is no error, we read from udp client
	goid := routine.Goid() //goid is used to identify goroutine and verify multithreading
	fmt.Printf("cur goid: %v\n", goid)
	for err == nil {
		n, remoteAddr, err = connection.ReadFromUDP(buffer)
		fmt.Print("-> ", string(buffer[0:n-1]))

		data := []byte(strconv.Itoa(random(1, 1001)))
		fmt.Printf("data sent: %s to client %s using the goroutine %d\n", string(data), remoteAddr.String(), goid)
		//send random number to client
		_, err = connection.WriteToUDP(data, remoteAddr)
		if err != nil {
			fmt.Println(err)
			return
		}
	}
	fmt.Println("listener failed - ", err)
	quit <- struct{}{}

}

func main() {
	arguments := os.Args
	if len(arguments) == 1 {
		fmt.Println("Please provide a port number!")
		return
	}
	PORT := ":" + arguments[1]

	//create the udp address with the port
	udpaddr, err := net.ResolveUDPAddr("udp4", PORT)
	if err != nil {
		fmt.Println(err)
		return
	}

	//listerner for udp server
	connection, err := net.ListenUDP("udp4", udpaddr)
	if err != nil {
		fmt.Println(err)
		return
	}

	defer connection.Close()

	rand.Seed(time.Now().Unix())

	//channel where goroutines have
	quit := make(chan struct{})
	//create light threads based on numcpu, btw is a fixed number of 'threads'
	for i := 0; i < runtime.NumCPU(); i++ {
		go listen(connection, quit)
	}
	<-quit // if there's an error in one client, the server is down
}
